# vue3-app

## Project setup
```
npm install
```

### Compiles and serves both server and client
```
npm run app
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Set up a .env file with configuration
```
MONGODB_CONNECTION='mongodb://<dbuser>:<dbpassword>@dsxxxxx.mlab.com:49065/<database>'
```
### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
