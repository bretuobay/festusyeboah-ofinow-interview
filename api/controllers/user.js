const bcryptHash = require('bcrypt').hashSync;
const bcryptSalt = require('bcrypt').genSaltSync;
const UserModel = require('../models/users');
const conn = require('../helpers/data-connection');

conn.connectToDatabase();

exports.saveNewUser = async ({ body: params }, res, next) => {
    console.log(params)
    const payload = {
        ...params,
        password: await bcryptHash(params.password, bcryptSalt(10))
    };

    try {

        const user = await UserModel.create(payload);
        res.json(user);

    } catch (error) {

        res.json({ 'error': error, 'message': 'Error creating new user.' });
        next(error);
    }
};


exports.getAllUsers = async (req, res, next) => {

    try {
        
        const users = await UserModel.find()
        res.json(users);

    } catch (error) {
        res.json({ 'error': error, 'message': 'Error retrieving registered users.' });
        next(error);
    }
};
