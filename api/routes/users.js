var express = require('express');
var router = express.Router();
var UsersController = require('../controllers/user');

// console.log(process.env.MONGODB_CONNECTION)

/* GET users listing. */
router.get('/', function (req, res, next) {
  UsersController.getAllUsers(req, res, next);
});

router.post('/save', function (req, res, next) {
  UsersController.saveNewUser(req, res, next);
});

module.exports = router;
