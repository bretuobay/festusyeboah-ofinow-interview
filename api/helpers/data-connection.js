const mongoose = require('mongoose');

const connectionOptions = {useNewUrlParser: true, useCreateIndex: true};

exports.connectToDatabase = async () => {
    try {
        const conn = await mongoose.connect(process.env.MONGODB_CONNECTION,connectionOptions);
        if (!conn) {
            throw Error(`Connection was not sucessful`);
        }
        console.log(`Coonected successfully to database`);
    } catch (error) {
        console.log(`Connection was not successful ${error}: ${process.env.MONGODB_CONNECTION}`);
    }
};

