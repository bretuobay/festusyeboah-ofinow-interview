//Require Mongoose
const mongoose = require('mongoose');

//Define a schema
const Schema = mongoose.Schema;

const UsersSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    lowercase: true,
    trim: true,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true,
    select: false
  },
});

// Compile model from schema
const UsersModel = mongoose.model('Users', UsersSchema);

module.exports = UsersModel;