const {
    expect
} = require("chai");
const {
    validateEmail,
    validateName,
    validatePassword
} = require('./validation-utils');


describe("Email checking suite", function () {
    it("should check if email string is valid", function () {
        expect(validateEmail("email@email.com").valid).to.equals(true);
    });

    it("should return false when string is not a valid email", function () {
        expect(validateEmail("email.email.com").valid).to.equals(false);
    });
});

describe("Name checking", function () {
    it("should check if name string is valid", function () {
        expect(validateName("").valid).to.equals(false);
    });

    it("should return false when string is not a valid name", function () {
        expect(validateName("Festus Yeboah").valid).to.equals(true);
    });
});

describe("Password checking", function () {
    it("should check if name string is valid", function () {
        expect(validatePassword("9439bbg").valid).to.equals(false);
    });

    it("should return false when string is not a valid name", function () {
        expect(validatePassword("supermegapassword").valid).to.equals(true);
    });
});
