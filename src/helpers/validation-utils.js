export const validateName = name => {
    if (!name.length) {
      return { valid: false, error: "This  name field is required." };
    }
    return { valid: true, error: null };
};
  
  export const validateEmail = email => {
    if (!email.length) {
      return { valid: false, error: "The email field is required." };
    }
    const emailRegex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
   
    if (!emailRegex.test(email)) {
      return { valid: false, error: "Please, enter a valid email." };
    }
    return { valid: true, error: null };
};
  
 export const validatePassword = password => {
    if (!password.length) {
      return { valid: false, error: "This passowrd field is required." };
    }
    if (password.length < 8) {
      return { valid: false, error: "The password entered is too short." };
    }
    return { valid: true, error: null };
};