const headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
};


export const fetchData = async (endpoint = '') => {
    try {
        const response = await fetch(endpoint, {
            mode: "cors"
        });
        if (!response.ok) {
            throw Error(response.statusText);
        }
        const values = await response.json()
        return {
            values,
            error: null
        };
    } catch (error) {
        return {
            values: null,
            error
        };
    }
};

export const postData = async (endPoint = '', payload) => {
    try {
        const response = await fetch(endPoint, {
            mode: "cors",
            headers,
            method: 'POST',
            body: JSON.stringify(payload)
        });
        if (!response.ok) {
            throw Error(response.statusText);
        }
        const values = await response.json();
        return {
            values,
            error: null
        };
    } catch (error) {
        return {
            values: null,
            error
        };
    }
};