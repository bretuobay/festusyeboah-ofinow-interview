import Vue from 'vue'
import Vuex from 'vuex'
import { USERS_API_ENDPOINT } from '../constants/api'
import { GET_ALL_USERS_SUCCESS, GET_ALL_USERS_FAILURE, POST_USER_SUCCESS, POST_USER_FAILURE } from '../constants/action-types'
import { fetchData, postData } from '../helpers/api-utils'

const state = {
    usersList: [],
    postUserError: '',
    getUsersError: '',
    lastRegisteredUser: {}
};

const actions = {
    getUsersList: async ({ commit }) => {
        const users = await fetchData(USERS_API_ENDPOINT);
        if (users.values) {
            commit(GET_ALL_USERS_SUCCESS, users);
        } else {
            commit(GET_ALL_USERS_FAILURE, users);
        }

    },
    postUser: async ({ commit }, payload) => {
        const users = await postData(`${USERS_API_ENDPOINT}/save`, payload);
        if (users.values) {
            commit(POST_USER_SUCCESS, users);
        } else {
            commit(POST_USER_FAILURE, users);
        }

    }
}

const getters = {
    getUsersListFromStore: ({ usersList }) => {
        return usersList;
    }
}

const mutations = {
    [GET_ALL_USERS_FAILURE](state, error) {
        state.getUsersError = error;
    },
    [POST_USER_FAILURE](state, error) {
        state.getUsersError = error;
    },
    [GET_ALL_USERS_SUCCESS](state, data) {
        state.usersList = data;
    },
    [POST_USER_SUCCESS](state, data) {
        state.lastRegisteredUser = data;
    }
}

//For modular structure later
const userStateModule = {
    state,
    getters,
    actions,
    mutations
}

Vue.use(Vuex)
Vue.config.debug = true;

export default new Vuex.Store({
    modules: {
        users: userStateModule
    }
})

