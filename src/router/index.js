import Vue from 'vue'
import VueRouter from 'vue-router'
import RegistrationPage  from '@/components/Register';
import UsersListPage  from '@/components/UsersList';

Vue.use(VueRouter);

const routes = [
  { path: '/', component: RegistrationPage },
  { path: '/register', component: RegistrationPage },
  { path: '/users-list', component: UsersListPage }
]

const router = new VueRouter({
  routes
})

export default router;